# README #

sd.bash is a BASH shell script used for changing directory like the 'cd' command, but it remembers the directories you have worked in and saves you typing when returning to those directories.  Please read the comments at the top of the file for setup instructions.
