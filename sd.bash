#!/bin/bash
#
#         sd (set directory) utility by Brien Barton [http://brispace.us]
#
#   To use SD, create an alias that executes this file:
#
#        alias sd="source path-to-this-file"
#
#   Then enter the command "sd -h" for help. 
#   Try "alias cd=sd" to continue using 'cd' but with the added 'sd' functionality.
#   You may customize sd by setting variables as follows (setting the first two as
#   shown will make sd act more like the cd command):
#
#   SDDEFARG=$HOME  # makes sd with no arguments take you to your 
#                   # home directory, otherwise it just shows 
#                   # the current directory
#
#   SDSILENT=1      # don't display new directory after change
#
#   SDHISTSIZE=30   # remember 30 most recent directories (default is 20)
#
#   SDAUTOSEARCH=1  # if directory not found, automatically search history
#
#   If you want to use sd on a regular basis, add code like the following to
#   your .bashrc file:
#
#        alias sd="source path-to-this-file"
#        alias cd=sd
#        SDAUTOSEARCH=1   # optionally set one or more of the variables above
#        shell_exit() {
#            sd -S  # save sd history for next session
#        }
#        trap shell_exit EXIT # execute shell_exit function on shell exit
#


# On first use this session, initialize variables
if [ -z "$SDVERSION" ]; then
  SDVERSION="1.2 02-Feb-2018"
  SDHISTHEADER="sd history file format 1"
  SDHIST=()   # Initialize the array that holds the directory history list
  SDHISTTIME=() # array holding time each entry was added to history list
  SDX=""      # stores the explicitly saved path (sd -s)
  SDPREV="$OLDPWD"   # stores the previous path
  SDIDX=0     # index of next storage slot in history array
  SDHISTFILE=~/.sd_history    # file to store history (see "sd -S")
  SDTEMPFILE=/tmp/sd_temp_$$       # temporary output file
  # Set default number of directories to remember (user may change variable value)
  SDHISTSIZE=${SDHISTSIZE:-20}
  sd -L > /dev/null    # load history from last session but hide any error msg
fi



# If no args, use default if given, else print current directory and return
if [ $# -lt 1 ]; then 
  if [ -n "$SDDEFARG" ]; then
      sd "$SDDEFARG"
  else
      pwd
  fi
  return 0
fi


# Process arguments
while [ $# -ge 1 ]
do
  sdarg=$1
  shift

  sdtmp=""
  sdfound=0
  i=0

  case $sdarg in

  %)      # Go to home directory
          sd "$HOME"
          ;;

  =[0-9]*)    # Display specified directory number
          sdtmp=${sdarg#=}
          sdtmp="${SDHIST[$sdtmp]}"
          if [ "$sdtmp" ]; then
              echo "$sdtmp"
          else
              echo "-sd: ${sdarg#=}: directory history entry not found"
          fi
          ;;

  =[p-]) # Display previous directory
          echo "$SDPREV"
          ;;

  =x|-x)    # Display or go to explicitly saved directory
          if [ "$SDX" ]; then
              if [ "$sdarg" = "=x" ]; then
                  echo "$SDX"
              else
                  sd "$SDX"
              fi
          else
              echo "-sd: you don't have a saved directory"
          fi
          ;;

  -|-p)     # Previous directory
          sd "$SDPREV"
          ;;

  -v)     # Show version
          echo "SD version $SDVERSION"
          ;;

  -s)     # Save current directory
          SDX="$PWD"
          SDXTIME=`date +'%Y%m%d%H%M%S'`
          echo "Saved directory $PWD.  Use command sd -x to return here."
          ;;

  -t)  # Display directory tree
          if [ -x /usr/bin/tree ]; then
              tree -d
          else
              ls -R | grep ':$' | sed -e 's/[^-][^\/]*\//|--/g;s/:$//;s/$|//;s/--|/  |/g'
          fi
          ;;

  -h)  # Help! 
  echo
  echo "SD remembers the last $SDHISTSIZE unique directories you visited."
  echo '
  Usage: 
      sd        ==> display current directory 
      sd dir    ==> goto directory "dir" 
      sd ~      ==> goto home directory
      sd %      ==> goto home directory
      sd ..     ==> go up one directory level
      sd ...[s] ==> goto subdirectory containing string s
      sd -      ==> go back to previous directory
      sd -[n]   ==> change to saved directory number n
      sd =[n]   ==> display saved directory number n
      sd -h     ==> display this help message
      sd -i [s] ==> display and select from saved directories
                    (optional: only those containing string s)
      sd -l [s] ==> display list of saved directories
                    (optional: only those containing string s)
      sd -p     ==> go back to previous directory
      sd -s     ==> explicitly save current directory
      sd -t     ==> display all subdirectories (tree)
      sd -x     ==> goto explicitly saved directory (see sd -s)
      sd -S     ==> save history to ~/.sd_histfile
      sd -L     ==> load history from ~/.sd_histfile
      sd -C     ==> clear out directory history from memory
      sd -CC    ==> remove directory history file

      '
      ;;

  -[0-9]*) # Select a directory by number
      sdtmp=${sdarg#-}
      sdtmp="${SDHIST[$sdtmp]}"
      if [ "$sdtmp" ]; then
          sd "$sdtmp"
      else
          echo "-sd: ${sdarg#-}: directory history entry not found"
      fi
      ;;

  -[li])    # Display list of saved directories
      if [ "$SDX" ]; then
          if [ ! "$1" ] || echo $SDX | grep -q "$1" ; then
              echo "x = $SDX (saved)"
              (( sdfound++ ))
          fi
      fi
      if [ "$SDPREV" ]; then
          if [ ! "$1" ] || echo $SDPREV | grep -q "$1" ; then
              echo "p = $SDPREV (previous)"
              (( sdfound++ ))
          fi
      fi
      for (( i=0; i < ${#SDHIST[*]}; i++ ))
      do
          sdtmp="${SDHIST[$i]}"
          if [ ! "$1" ] || echo $sdtmp | grep -q "$1" ; then
              echo "$i = $sdtmp"
              (( sdfound++ ))
          fi
      done
      if [[ "$1" && $sdfound -eq 0 ]]; then
          echo "-sd: string '${1}' not found in directory history"
      fi      
      if [[ "$sdarg" = "-i" && $sdfound -gt 0 ]]; then
          echo ""
          echo -n "Enter a directory number: "; read sdtmp
          if [ "x$sdtmp" != "x" ] 
          then
              case "x$sdtmp" in
                  xx) sd "$SDX"   ;;
                  xp) sd "$SDPREV"    ;;
                  x[0-9]*)  sd -$sdtmp  ;;
              esac
          fi
      fi

      break  # ignore any additional arguments
      ;;

  ...*)    # Search for subdirectory
      sdtmp=${sdarg#...}    # remove the ellipses
      find -L . -type d -print 2>/dev/null | grep "${sdtmp}" > $SDTEMPFILE
      sdtmp=()    # initialize an empty array
      { 
          while read line
          do
              sdtmp+=("$line")
          done
      } < $SDTEMPFILE # fill array with any directories we found
      # Use rm -f so we don't get prompted if rm aliased to 'rm -i'
      rm -f $SDTEMPFILE
      case ${#sdtmp[*]} in
        1) sd "$sdtmp" ;;     # single match, go there
        0) echo "Directory '${sdarg#...}' not found below $PWD" ;;
        *) echo "Multiple matches: "
           for (( i=0; i < ${#sdtmp[*]}; i++ ))
           do
               echo "$i = ${sdtmp[$i]}"
           done
           echo -n "Enter number of desired directory: "
           read n
           # Make sure it's a valid choice before using it
           for (( i=0; i < ${#sdtmp[*]}; i++ ))
           do
               if [ "x$i" = "x$n" ]
               then
                   sd "${sdtmp[$n]}"
                   break
               fi
           done
           ;;
      esac
      ;;

  -S) # Save history to file
      echo "`date +'%Y%m%d%H%M%S'`:${PWD}" > $SDTEMPFILE  # include current directory
      # Save history entries
      for ((i = 0; i < ${#SDHIST[@]}; i++))
      do
          echo "${SDHISTTIME[$i]}:${SDHIST[$i]}" >> $SDTEMPFILE
      done
      # Merge in entries in previously saved history (skip over version, SDX and SDPREV lines)
      if [ -f "$SDHISTFILE" ]; then
          if [ "$SDHISTHEADER" = "`head -1 $SDHISTFILE`" ]; then # if compatible version
              tail -n +4 "$SDHISTFILE" >> $SDTEMPFILE
          fi
      fi
      # First descending sort on directories and times so matching directories 
      # will be adjacent with newest first, 2nd sort removes duplicate (older)
      # paths, 3rd sort gets our expected ordering of oldest to newest, and tail
      # makes sure we have no more than SDHISTSIZE of the newest paths.
      sort -t: -k2 -k1 -r $SDTEMPFILE | sort -t: -k2 -u | sort -t: -k1 | \
         tail -n $SDHISTSIZE > ${SDTEMPFILE}_2

      # Create new history file
      echo "$SDHISTHEADER" > $SDHISTFILE
      echo "${SDXTIME}:${SDX}" >> $SDHISTFILE
      echo "${SDPREVTIME}:${SDPREV}" >> $SDHISTFILE
      cat ${SDTEMPFILE}_2 >> $SDHISTFILE
      rm -f $SDTEMPFILE ${SDTEMPFILE}_2
      ;;

  -L) # Load history file
      # If history file exists and is compatible
      if [ -f "$SDHISTFILE" ]; then
          if [ "$SDHISTHEADER" = "`head -1 $SDHISTFILE`" ]; then
              SDHIST=()
              SDHISTTIME=()
              {
                  read sdtmp          # version line
                  read sdtmp          # SDX time and value
                  SDXTIME=${sdtmp%%:*} # remove colon to end of string
                  SDX=${sdtmp#*:}      # remove up to and including colon
                  read sdtmp          # SDPREV time and value
                  SDPREVTIME=${sdtmp%%:*}
                  SDPREV=${sdtmp#*:}
                  SDIDX=0
                  while read x
                  do
                      xtime=${x%%:*}
                      xpath=${x#*:}
                      SDHIST+=("$xpath")
                      SDHISTTIME+=("$xtime")
                      ((SDIDX += 1))
                      if [ $SDIDX -ge $SDHISTSIZE ]; then
                          SDIDX=0
                          break
                      fi
                  done
              } < $SDHISTFILE
          else
              echo "-sd: -L (load) error: history file is incompatible with this version of sd"
          fi
      else
          echo "-sd: -L (load) error: no sd history file found"
      fi
      ;;

  -C) # Clear history
      SDHIST=()
      SDHISTTIME=()
      SDIDX=0
      SDPREV=""
      SDPREVTIME=""
      SDX=""
      SDXTIME=""
      ;;
    
  -CC) # Clear history file
      rm -i ${SDHISTFILE}
      ;;

  *)    # Assume argument is a directory and attempt to go there
      sdtmp="$PWD"
      builtin cd "$sdarg" >/dev/null
      # If invalid directory
      if [ $? != 0 ] 
      then
          # If AUTOSEARCH option enabled then look for match else error
          if [ "$SDAUTOSEARCH" ]; then
              sd -i "$sdarg"
              break
          else
              return  # Error!
          fi
      fi
      if [ -z "$SDSILENT" ]; then echo $PWD; fi
      # Don't change saved last directory if we haven't moved
      if [ "$sdtmp" != "$PWD" ] 
      then
          SDPREV="$sdtmp" # save previous directory
          SDPREVTIME=`date +'%Y%m%d%H%M%S'`
          # Check if we have the previous directory in our history array
          for ((i = 0; i < ${#SDHIST[@]}; i++))
          do
              if [ "${SDHIST[$i]}" = "$sdtmp" ]; then
                  sdfound=1
                  break
              fi
          done
          if [ $sdfound -ne 1 ]; then
              eval "SDHIST[$SDIDX]='$sdtmp'"
              eval "SDHISTTIME[$SDIDX]=`date +'%Y%m%d%H%M%S'`"
              (( SDIDX++ ))
              if [ $SDIDX -ge $SDHISTSIZE ]; then SDIDX=0; fi
          fi
      fi
  esac
done

